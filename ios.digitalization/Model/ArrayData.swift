//
//  Array.swift
//  ios.digitalization
//
//  Created by Akarys Turganbekuly on 06.09.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit


public class ArrayData {
    
    let AktTehnObsled = ["аварийный","неаварийный"] // bool
    let BalansovayaPrenadlejnost = ["Государственная Собственность","Частная Собственность"]
    let Oblicovka = ["Monolith","Brick","Blocks","Facing"]
    let SostoyanieStenDoma = ["Удволетворительное","Неудволетворительное"] //bool
    let MaterialFundamenta = ["Pile","Material","Banded","Monolith","Building Blocks", "Other"]
    let SostoyanieFundamenta = ["Удволетворительное","Неудволетворительное"] //bool
    let SteniPodvalaDoma = ["Material","Building Blocks","Monlith","Others"]
    let SostoyanieStenDomaOpt = ["Удволетворительное","Неудволетворительное"] //bool
    let PerekritiePodvala = ["Material","Reinforced concrete","Monlith","Wood"]
    let SostoyaniePerekritiya = ["Удволетворительное","Неудволетворительное"] //bool
    let KriwaTip = ["Pitched","Flat","Other"]
    let KriwaMaterial = ["Wood"]
    let KrovlyaMaetiral = ["Metal","Shingles","Slate","Reberoid"]
    let MusoroprovodSostoyanie = ["Whole","Destroyed"]
    let PodezdnieOknaMaterial = ["Wood","Plastic"]
    let NalichieLifta = ["Да","Нет"] //bool
    let VidTepolosnobjeniya = ["Open","Closed"]
    let tipTepolosnobjeniya = ["Autonomus", "Central", "Stove"]
    let MaterialVodoprovoda = ["Metal","Plastic"]
    let SostoyanieVopdoprovoa = ["Emergency","Not Emergency","Corrosion"]
    let MaterialTruboprovoda = ["Metal","Plastic"]
    let SostoyanieTruboprovoda = ["Emergency","Not Emergency","Corrosion"]
    let TokoprovodyawieJili = ["Copper","Aluminum","SHRS"]
    
}
