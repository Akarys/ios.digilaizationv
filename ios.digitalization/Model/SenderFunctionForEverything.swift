//
//  SenderFunctionForEverything.swift
//  ios.digitalization
//
//  Created by Akarys Turganbekuly on 05.09.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit
import JSONRPCKit
import APIKit
import SwiftyJSON

func odooAuthenticate(db:String, login:String, password:String, completion: @escaping (_ result: JSON)->()) {
    let batchFactory = BatchFactory(version: "2.0", idGenerator: NumberIdGenerator())
    let request = RPCAuthenticationRequest(o_db: db, o_login: login, o_password: password)
    let batch = batchFactory.create(request)
    let httpRequest = AuthenticationServiceRequest(batch: batch)
    
    Session.send(httpRequest) { result in
        switch result {
        case .success(let response):
            let json = JSON(response)
            completion(json)
        case .failure(let error):
            print("Error: ", error)
            completion(JSON.null)
        }
    }
}


func getResponse(o_method:String, o_model:String, o_domain:Any?, o_fields:Any?, completion: @escaping (_ result: JSON)->()) {
    let batchFactory = BatchFactory(version: "2.0", idGenerator: NumberIdGenerator())
    let request = RPCRequest(o_method: o_method, o_model: o_model, o_domain: o_domain, o_fields: o_fields)
    let batch = batchFactory.create(request)
    let httpRequest = MyServiceRequest(batch: batch)
    
    Session.send(httpRequest) { result in
        switch result {
        case .success(let response):
            let json = JSON(response)
            completion(json)
        case .failure(let error):
            print("Error: ", error)
            completion(JSON.null)
        }
    }
}



func uploadRecord(o_model:String, attrs: Dictionary<String,Any>, completion: @escaping (_ result: JSON)->()){
    let params:[String:Any]
    params = ["kwargs":["context":[]], "args":[attrs], "method":"create", "model":o_model, "context":[]]
    let batchFactory = BatchFactory(version: "2.0", idGenerator: NumberIdGenerator())
    let request = RPCGeneralReq(params: params)
    let batch = batchFactory.create(request)
    let httpRequest = MyServiceRequest(batch: batch)
    
    Session.send(httpRequest) { result in
        switch result {
        case .success(let response):
            let json = JSON(response)
            completion(json)
        case .failure(let error):
            print("Error: ", error)
            completion(JSON.null)
        }
    }
}
