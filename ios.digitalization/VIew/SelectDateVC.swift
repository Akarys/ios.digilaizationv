//
//  SelectDateVC.swift
//  ios.digitalization
//
//  Created by Akarys Turganbekuly on 06.09.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit

class SelectDateVC: UIViewController {

    let datePicker = datePopUp()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        // Do any additional setup after loading the view.
        view.addSubview(datePicker)
    }


}
