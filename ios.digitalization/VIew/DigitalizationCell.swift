//
//  DigitalizationCell.swift
//  ios.digitalization
//
//  Created by Akarys Turganbekuly on 05.09.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit

class DigitalizationCell: UITableViewCell {
    
    let jkname: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    let jkimage: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "LOOOL")
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    
    let mkrname: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.jkimage.image = nil
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(jkimage)
        jkimage.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 60, height: 60)
        jkimage.layer.cornerRadius = 60/2
        jkimage.clipsToBounds = true
        setupName()
    }
    
    func setupName() {
        let stackView = UIStackView(arrangedSubviews: [jkname, mkrname])
        
        stackView.axis = .vertical
        stackView.spacing = 3
        stackView.distribution = .fillEqually
        
        addSubview(stackView)
        stackView.anchor(top: nil, left: jkimage.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Fatal error")
    }
}
