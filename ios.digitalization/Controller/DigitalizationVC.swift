//
//  DigitalizationVC.swift
//  ios.digitalization
//
//  Created by Akarys Turganbekuly on 05.09.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit
import JSONRPCKit
import APIKit
import SwiftyJSON
import JJFloatingActionButton
import Matisse

var jkID: Int = 0

class DigitalizationVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    let tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    var data = [resUser]()
    var nameUser = [namesss]() {
        didSet{
            print(nameUser)
        }
    }
    
    var nameIS: String = ""
    
    
    var cellId = "digitcell"
    
    fileprivate let actionButton = JJFloatingActionButton()
    
    let headerView: UIView = {
        let header = UIView()
        header.backgroundColor = UIColor.rgb(red: 77, green: 146, blue: 67)
        return header
    }()
    
    let headeriv: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "LOOOL")
        return iv
    }()
    
    let headerlabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(headerView)
        view.addSubview(tableView)
        tableView.frame = view.frame
        tableView.delegate = self
        tableView.dataSource = self
        
//        if isLoggedIn() {
//            //assume user is logged in
//            let homeController = DigitalizationVC()
//            navigationController?.pushViewController(homeController.self, animated: true)
//            
//        } else {
//            perform(#selector(showLoginController), with: nil, afterDelay: 0.01)
//        }
        
        
        headerView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)
        self.edgesForExtendedLayout = []
        tableView.anchor(top: headerView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        tableView.register(DigitalizationCell.self, forCellReuseIdentifier: cellId)
        
        getJSONresponse()
        
        setupNavigationBar()
        
        addFloatingButton()
        setnameRPC()
        
        setHeader()
    }
    fileprivate func isLoggedIn() -> Bool {
        return UserDefaults.standard.isLoggedIn()
    }
    
    @objc func showLoginController() {
        let loginController = ViewController()
        present(loginController, animated: true, completion: {
            //perhaps we'll do something here later
        })
    }
    
    // MARK:- Put that func always on the first place
    func getJSONresponse() {
        getResponse(o_method: "search_read", o_model: "res.users", o_domain:[["id", "=", user_id]], o_fields: ["name","partner_id"]) { (result) -> () in
            //print(result)
            if result != JSON.null {
                let partner_id = result[0]["partner_id"][0].int!
                getResponse(o_method: "search_read", o_model: "res.partner", o_domain: [["id", "=", partner_id]], o_fields: ["name", "kato"], completion: { (result)->() in
                    //print(result)
                    let id = result[0]["kato"][0].int!
                    
                    getResponse(o_method: "search_read", o_model: "property.building", o_domain: [["kato", "=", id]], o_fields: ["name", "region"], completion: { (result)->() in
                        
                        result.forEach({ (temp) in
                            
                            var area:String = "", name:String = "", totID: Int = 0
                            
                            if (temp.1["region"].string != nil) {
                                area = temp.1["region"].string!
                            }
                            if (temp.1["name"].string != nil) {
                                name = temp.1["name"].string!
                            }
                            if (temp.1["id"].int != nil) {
                                totID = temp.1["id"].int!
                            }
                            
                            let newModel = resUser(area: area, name: name, id: totID)
                            self.data.append(newModel)
                        })
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        //                        print(self.data)
                    })
                })
            }
        }
    }
    
    func setnameRPC() {
        //        var name: String = ""
        //        var image: String = ""
        
        
        
    }
    func setHeader() {
        
        headerView.addSubview(headeriv)
        headerView.addSubview(headerlabel)
        getResponse(o_method: "search_read", o_model: "res.users", o_domain: [["id", "=", user_id]], o_fields: ["name"]) { (result) in
            
            
            guard let name = result[0]["name"].string else{return}
            
            let newUser = namesss(name: name)
            self.nameUser.append(newUser)
            self.nameIS = newUser.name!
            print(self.nameIS + "ASDGAMER" )
            print(self.nameUser)
            print("++++++++++++++++++++++++++++++++++")
            self.headerlabel.text = "         Добро Пожаловать!\n\(self.nameIS)"
            
        }
        
        let imageURLs = URL(string: "http://shanyraq.valis.kz/web/image?model=res.users&id=\(user_id!)&field=image_small")!
        Matisse.load(imageURLs)
            .showIn(headeriv)
        print(imageURLs)
        
        headeriv.anchor(top: headerView.topAnchor, left: headerView.leftAnchor, bottom: nil, right: nil, paddingTop: 12, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 80, height: 80)
        headeriv.layer.cornerRadius = 80 / 2
        headeriv.clipsToBounds = true
        
        headerlabel.anchor(top: headerView.topAnchor, left: headeriv.rightAnchor, bottom: headerView.bottomAnchor, right: headerView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        headerlabel.textColor = .white
        
        
    }
    
    func addFloatingButton(){
        actionButton.addItem(title: "Home", image: UIImage(named: "home_selected")?.withRenderingMode(.alwaysTemplate)) { item in
            Helper.showAlert(for: item)
            
        }
        
        actionButton.addItem(title: "Like", image: #imageLiteral(resourceName: "plus_unselected")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.addItem(title: "Balloon", image: #imageLiteral(resourceName: "home_selected")) { item in
            let createVC = CreateMzhdVC()
            self.navigationController?.pushViewController(createVC.self, animated: true)
        }
        
        actionButton.display(inViewController: self)
    }
    
    func setupNavigationBar() {
        navigationItem.title = "Жилые Комплексы"
        navigationController?.navigationBar.barTintColor = UIColor.rgb(red: 77, green: 146, blue: 67)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 20)]
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "digitcell", for: indexPath) as! DigitalizationCell
        let item = data[indexPath.row]
        let imageURLs = URL(string: "http://shanyraq.valis.kz/web/image?model=property.building&id=\(item.id)&field=build_main_photo")!
        Matisse.load(imageURLs)
            .showIn(cell.jkimage)
        print(imageURLs)
        
        cell.jkname.text = item.area
        cell.mkrname.text = item.name
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(self.data[indexPath.row].id)
        
        jkID = data[indexPath.row].id
        
        let transitionView = DisplayBuildingInfo()
        transitionView.idwka = jkID
        navigationController?.pushViewController(transitionView.self, animated: true)
    }
}
