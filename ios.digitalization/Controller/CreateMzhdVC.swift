//
//  CreateMzhdVC.swift
//  ios.digitalization
//
//  Created by Akarys Turganbekuly on 06.09.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit
import JSONRPCKit
import APIKit
import SwiftyJSON
import JJFloatingActionButton
import Matisse

class CreateMzhdVC: UIViewController {
    
    lazy var ScrollView: UIScrollView = {
        
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.contentSize.height = 5000
        return scrollView
    }()
    private var DatePicker: UIDatePicker?
    
    let glavniiLab: UILabel = {
        let label = UILabel()
        label.text = "Главный Фасад"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let dvorovoiLab: UILabel = {
        let label = UILabel()
        label.text = "Дворовой Фасад"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let leviiLab: UILabel = {
        let label = UILabel()
        label.text = "Левый торец фасада"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let praviiLab: UILabel = {
        let label = UILabel()
        label.text = "Правый торец фасада"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    
    
    // Adress information
    let adress: UILabel = {
        let label = UILabel()
        label.text = "Адрес"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let ulica: UILabel = {
        let label = UILabel()
        label.text = "Улица"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let nomerdoma: UILabel = {
        let label = UILabel()
        label.text = "Номер дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let index: UILabel = {
        let label = UILabel()
        label.text = "Индекс"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let oblast: UILabel = {
        let label = UILabel()
        label.text = "Область"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let gorod: UILabel = {
        let label = UILabel()
        label.text = "Город"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    //Common information about MZHD
    let ObwnieSvedeniyaMZHD: UILabel = {
        let label = UILabel()
        label.text = "Общие сведения"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let InvertarniiNomer: UILabel = {
        let label = UILabel()
        label.text = "Инвентарный номер дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KadastroviiNomer: UILabel = {
        let label = UILabel()
        label.text = "Кадастровый номер дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let RegestraciyaKondominimuma: UILabel = {
        let label = UILabel()
        label.text = "Регистрация Кондоминимума"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataRegestraciyaKondominimuma: UILabel = {
        let label = UILabel()
        label.text = "Дата Регистрации Кондоминимума"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ObwayaPlowadUchastka: UILabel = {
        let label = UILabel()
        label.text = "Общая площадь земельного участка"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PosledniiKapitalniiRemont: UILabel = {
        let label = UILabel()
        label.text = "Последний капитальный ремонт"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let AktTehnObsledovaniya: UILabel = {
        let label = UILabel()
        label.text = "Акт технического обследования"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let BalansPrinadlej: UILabel = {
        let label = UILabel()
        label.text = "Балансовая принадлежность"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let InformacionnayaSystema: UILabel = {
        let label = UILabel()
        label.text = "Информационная Система"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let InformacionnayaIPadress: UILabel = {
        let label = UILabel()
        label.text = "IP адрес"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let EnergoAudiot: UILabel = {
        let label = UILabel()
        label.text = "Энергоаудит"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KlassEnergo: UILabel = {
        let label = UILabel()
        label.text = "Класс Энергоэффективности"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let TehPasport: UILabel = {
        let label = UILabel()
        label.text = "Техпаспорт"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    //Tehnicheskaya harakteristika
    let Tehnharak: UILabel = {
        let label = UILabel()
        label.text = "Техническая Характеристика"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let KolvoKvartir: UILabel = {
        let label = UILabel()
        label.text = "Количество квартир"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let GodPostroiki: UILabel = {
        let label = UILabel()
        label.text = "Год постройки"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ObwayaPlowadDoma: UILabel = {
        let label = UILabel()
        label.text = "Общая площадь дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ZhilayaPlowadDoma: UILabel = {
        let label = UILabel()
        label.text = "Жилая площадь дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadNejelih: UILabel = {
        let label = UILabel()
        label.text = "Площадь нежелих помещений"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadPodvala: UILabel = {
        let label = UILabel()
        label.text = "Площадь подвала"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadCherdaka: UILabel = {
        let label = UILabel()
        label.text = "Площадь чердака"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadTehnEtaja: UILabel = {
        let label = UILabel()
        label.text = "Площадь технического этажа"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoEtajei: UILabel = {
        let label = UILabel()
        label.text = "Количество этажей"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoPodezdov: UILabel = {
        let label = UILabel()
        label.text = "Количество подъездов"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoSekcii: UILabel = {
        let label = UILabel()
        label.text = "Количество секций"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadMansardi: UILabel = {
        let label = UILabel()
        label.text = "Площадь мансарды"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadParkinga: UILabel = {
        let label = UILabel()
        label.text = "Площадь паркинга"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SrokSlujbZdaniya: UILabel = {
        let label = UILabel()
        label.text = "Срок службы здания"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let IznosMZHD: UILabel = {
        let label = UILabel()
        label.text = "Износ МЖД"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    //Narujnie steni
    let NarujnieSteni: UILabel = {
        let label = UILabel()
        label.text = "Наружные стены"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let NarujnieSteniIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let Oblicovka: UILabel = {
        let label = UILabel()
        label.text = "Облицовка"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieNaruknihsten: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let FasadNarujnihSten: UILabel = {
        let label = UILabel()
        label.text = "Фасад"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieKato: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    // Fundament Opisanie
    
    let FundamentLabel: UILabel = {
        let label = UILabel()
        label.text = "Фундамент"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let FundamentIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialFundament: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieFundament: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Steni Podvala
    
    let SteniPodvalaLabel: UILabel = {
        let label = UILabel()
        label.text = "Стены Подвала"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let SteniPodvalaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialStenPodvala: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieStenPodvala: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    // Perekritie
    let PerekritiyaLabel: UILabel = {
        let label = UILabel()
        label.text = "Перекрытия"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let PerekritieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialPerekritiya: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyaniePerekritiya: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    //Lestnici
    let LestniciLabel: UILabel = {
        let label = UILabel()
        label.text = "Лестницы"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let LestniciIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let SostoyanieLestnic: UILabel = {
        let label = UILabel()
        label.text = "Состояние Лестниц"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyaniePeril: UILabel = {
        let label = UILabel()
        label.text = "Состояние Перил"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    // Балконныыыы
    
    let BalkoniLabel: UILabel = {
        let label = UILabel()
        label.text = "Балконы"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let BalkoniIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let SostoyanieBalkonov: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Крыша
    let KriwaLabel: UILabel = {
        let label = UILabel()
        label.text = "Крыша"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KriwaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let TipKriw: UILabel = {
        let label = UILabel()
        label.text = "Тип"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MaterialKriw: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieKRiwi: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    // Кровля
    
    let KrovlyaLabel: UILabel = {
        let label = UILabel()
        label.text = "Кровля"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let KrovlyaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialKrovlya: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieKrovlya: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Мусоропровод
    let MusoroprovodLabel: UILabel = {
        let label = UILabel()
        label.text = "Мусоропровод"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let MusoroprovodIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let NalichieMusoroprovoda: UILabel = {
        let label = UILabel()
        label.text = "Наличие"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieMusoroprovoda: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    // Podezdi
    let PodezdiLabel: UILabel = {
        let label = UILabel()
        label.text = "Подъезды"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let PodezdiIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let SostoyaniePodezd: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ElektroLampi: UILabel = {
        let label = UILabel()
        label.text = "Электросберигающие лампы"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Podezdnie Okna
    let PodezdnieOknaLabel: UILabel = {
        let label = UILabel()
        label.text = "Подъездные окна"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let PodezdnieOknaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let KolvoOkon: UILabel = {
        let label = UILabel()
        label.text = "Количество"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MaterialOkon: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ///////////////////////Liftiiii
    let LiftiLabel: UILabel = {
        let label = UILabel()
        label.text = "Лифты"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let LiftiIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let NalichieLifta: UILabel = {
        let label = UILabel()
        label.text = "Наличие"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoLiftov: UILabel = {
        let label = UILabel()
        label.text = "Количество"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let StranaIzgLifta: UILabel = {
        let label = UILabel()
        label.text = "Страна-изготовитель"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SrokEkspluatLifta: UILabel = {
        let label = UILabel()
        label.text = "Срок Эксплуатации"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataUstanovkiLifta: UILabel = {
        let label = UILabel()
        label.text = "Дата Установки"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataPoslObslLifta: UILabel = {
        let label = UILabel()
        label.text = "Дата последнего обследования"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ObslujOrganizaciya: UILabel = {
        let label = UILabel()
        label.text = "Обслуживащая Организация"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let TipOplatiLifta: UILabel = {
        let label = UILabel()
        label.text = "Тип оплаты"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //////////////////////////// ТЕПЛОСНАБЖЕНИЕ
    
    let TeplosabjenieLabel: UILabel = {
        let label = UILabel()
        label.text = "Теплоснабжение"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let TeplosnabjenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let TipTeplosnab: UILabel = {
        let label = UILabel()
        label.text = "Тип"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NomerCTP: UILabel = {
        let label = UILabel()
        label.text = "Номер ЦТП"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoTeplovihVvodov: UILabel = {
        let label = UILabel()
        label.text = "Количество тепловых вводов"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DiametrTruboProvoda: UILabel = {
        let label = UILabel()
        label.text = "Диаметр Трубопровода"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieATP: UILabel = {
        let label = UILabel()
        label.text = "Наличие АТП"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MarkaATP: UILabel = {
        let label = UILabel()
        label.text = "Марка АТП"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieOPUTE: UILabel = {
        let label = UILabel()
        label.text = "Наличие ОПУТЭ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoOPUTE: UILabel = {
        let label = UILabel()
        label.text = "Количество ОПУТЭ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MarkaOPUTE: UILabel = {
        let label = UILabel()
        label.text = "Марка ОПУТЭ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataPoslProverki: UILabel = {
        let label = UILabel()
        label.text = "Дата последней проверки"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodderjkaDistancionnoi: UILabel = {
        let label = UILabel()
        label.text = "Поддержка Дистанционной"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieNasosa: UILabel = {
        let label = UILabel()
        label.text = "Наличие Насоса"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ////////////////////// VODOPROVOD
    let VodoprovodLabel: UILabel = {
        let label = UILabel()
        label.text = "Водопровод"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let VodoprovodIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialVopdoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DiametrVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Диаметр Водопровода"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MarkaOPU: UILabel = {
        let label = UILabel()
        label.text = "Марка ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieOPU: UILabel = {
        let label = UILabel()
        label.text = "Наличие ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodderjkaDistancionnoiVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Поддержка Дистанционной"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieNasosaVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Наличие Насоса"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ///////////////////VODOOTVEDENIE
    let VodootvedenieLabel: UILabel = {
        let label = UILabel()
        label.text = "Водоотведение"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let VodootvedenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialVodootvedenie: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DiametrVodoprovodaVodootvet: UILabel = {
        let label = UILabel()
        label.text = "Диаметр Водопровода"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieVodoprovodaVodootvet: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ///////////////////////////GAZOSNOBJENIE
    let GazosnobjenieLabel: UILabel = {
        let label = UILabel()
        label.text = "Газоснобжение"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let GazosnabjenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let NalichieGazosnobjeniya: UILabel = {
        let label = UILabel()
        label.text = "Наличие"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    /////////////////////////////ELEKTROSNABJENIE
    let ElektrosnobjenieLabel: UILabel = {
        let label = UILabel()
        label.text = "Наличие Насоса"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    let ElektrosnabjenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MarkaOPUElektro: UILabel = {
        let label = UILabel()
        label.text = "Марка ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieOPUElektro: UILabel = {
        let label = UILabel()
        label.text = "Наличие ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodderjkaDistancionnoiElektrosnobjenie: UILabel = {
        let label = UILabel()
        label.text = "Поддержка Дистанционной"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    let SostoyanieWitovih: UILabel = {
        let label = UILabel()
        label.text = "Состояние щитовых"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let TokoprovodyawieJili: UILabel = {
        let label = UILabel()
        label.text = "Токопроводящие жилы кабелей"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let WRC: UILabel = {
        let label = UILabel()
        label.text = "ШРС"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //////////// POTREBLYAEMAYA MOWNOST DOMA
    let PotreblyaemayaMownostLabel: UILabel = {
        let label = UILabel()
        label.text = "Потребляемая мощность дома"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        return label
    }()
    
    
    /////////////////////////////////////////////
   ///////////////////////////////////////////// RESPONSE FROM RPC
    // Adress information
    let ulicaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NomerdomaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let IndexResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let oblastResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let gorodResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    //Common information about MZHD
    let InvertarniiNomerResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let InformacionnayaIPadressResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    let KadastroviiNomerResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let RegestraciyaKondominimumaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let DataRegestraciyaKondominimumaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    let ObwayaPlowadUchastkaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PosledniiKapitalniiRemontResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let AktTehnObsledovaniyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let BalansPrinadlejResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let InformacionnayaSystemaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let EnergoAudiotResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let KlassEnergoResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let TehPasportResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    //Tehnicheskaya harakteristika
    let KolvoKvartirResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let GodPostroikiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let ObwayaPlowadDomaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let ZhilayaPlowadDomaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PlowadNejelihResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PlowadPodvalResponsea: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PlowadCherdakaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PlowadTehnEtajaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let KolvoEtajeiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let KolvoPodezdovResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let KolvoSekciiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PlowadMansardiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PlowadParkingaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SrokSlujbZdaniyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let IznosMZHDResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    //Narujnie steni
    let OblicovkaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieNaruknihstenResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let FasadNarujnihResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieKatoResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    // Fundament Opisanie
    let MaterialFundamentResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieFundamentResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    //Steni Podvala
    let MaterialStenPodvalaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieStenPodvalaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    // Perekritie
    
    let MaterialPerekritiyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyaniePerekritiyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    //Lestnici
    let SostoyanieLestnicResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyaniePerilResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    // Балконныыыы
    
    let SostoyanieBalkonovResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    //Крыша
    let TipKriwResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let MaterialKriwResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieKRiwiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    // Кровля
    
    let MaterialKrovlyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieKrovlyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    //Мусоропровод
    let NalichieMusoroprovodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieMusoroprovodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    // Podezdi
    let SostoyaniePodezdResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let ElektroLampiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    //Podezdnie Okna
    let KolvoOkonResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let MaterialOkonResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    ///////////////////////Liftiiii
    let NalichieLiftaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let KolvoLiftovResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let StranaIzgLiftaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SrokEkspluatLiftaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let DataUstanovkiLiftaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let DataPoslObslLiftaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let ObslujOrganizaciyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let TipOplatiLiftaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    //////////////////////////// ТЕПЛОСНАБЖЕНИЕ
    
    let TipTeplosnabResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NomerCTPResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let KolvoTeplovihVvodovResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let DiametrTruboProvodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NalichieATPResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let MarkaATPResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NalichieOPUTEResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let KolvoOPUTEResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let MarkaOPUTEResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let DataPoslProverkiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PodderjkaDistancionnoiResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NalichieNasosaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    ////////////////////// VODOPROVOD
    let MaterialVopdoprovodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let DiametrVodoprovodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieVodoprovodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let MarkaOPUResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NalichieOPUResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PodderjkaDistancionnoiVodoprovodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NalichieNasosaVodoprovodaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    ///////////////////VODOOTVEDENIE
    let MaterialVodootvedenieResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let DiametrVodoprovodaVodootvetResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let SostoyanieVodoprovodaVodootvetResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    ///////////////////////////GAZOSNOBJENIE
    
    let NalichieGazosnobjeniyaResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    /////////////////////////////ELEKTROSNABJENIE
    let MarkaOPUElektroResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let NalichieOPUElektroResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let PodderjkaDistancionnoiElektrosnobjenieResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    let SostoyanieWitovihResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let TokoprovodyawieJiliResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    let WRCResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    //////////// POTREBLYAEMAYA MOWNOST DOMA
    let PotreblyaemayaMownostResponse: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Введите улицу"
        return textfield
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(ScrollView)
        ScrollView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        setupViews()
        
            
            
//        DatePicker = UIDatePicker()
//        DatePicker?.datePickerMode = .date
//        DatePicker?.addTarget(self, action: #selector(CreateMzhdVC.dateChanged(DatePicker:)), for: .valueChanged)
//
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CreateMzhdVC.viewTapped(gestureRecognizer:)))
//        view.addGestureRecognizer(tapGesture)
//
//        DataRegestraciyaKondominimumaResponse.inputView = DatePicker
//        PosledniiKapitalniiRemontResponse.inputView = DatePicker
//        EnergoAudiotResponse.inputView = DatePicker
//        GodPostroikiResponse.inputView = DatePicker
    }
    
//    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
//        view.endEditing(true)
//    }
//
//    @objc func dateChanged(DatePicker: UIDatePicker) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd/MM/yyyy"
//
//        DataRegestraciyaKondominimumaResponse.text = dateFormatter.string(from: DatePicker.date)
//        PosledniiKapitalniiRemontResponse.text = dateFormatter.string(from: DatePicker.date)
//        EnergoAudiotResponse.text = dateFormatter.string(from: DatePicker.date)
//        GodPostroikiResponse.text = dateFormatter.string(from: DatePicker.date)
//        view.endEditing(true)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc fileprivate func handleUItfPicker(){
        let selectVC = UINavigationController(rootViewController: SelectDateVC())
        selectVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        present(selectVC, animated: true, completion: nil)
    }
    
    fileprivate func setupViews() {
        ScrollView.addSubview(adress)
        adress.anchor(top: ScrollView.topAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewAddress = UIStackView(arrangedSubviews: [oblast, oblastResponse,gorod, gorodResponse,ulica, ulicaResponse,nomerdoma, NomerdomaResponse,index, IndexResponse])
        stackViewAddress.axis = .vertical
        stackViewAddress.distribution = .fillEqually
        stackViewAddress.spacing = 7
        ScrollView.addSubview(stackViewAddress)
        stackViewAddress.anchor(top: adress.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 250)
        
        

        ScrollView.addSubview(ObwnieSvedeniyaMZHD)
        ObwnieSvedeniyaMZHD.anchor(top: stackViewAddress.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewObwieSvedeniya = UIStackView(arrangedSubviews: [InvertarniiNomer, InvertarniiNomerResponse,KadastroviiNomer, KadastroviiNomerResponse,RegestraciyaKondominimuma, RegestraciyaKondominimumaResponse,DataRegestraciyaKondominimuma, DataRegestraciyaKondominimumaResponse,ObwayaPlowadUchastka, ObwayaPlowadUchastkaResponse,PosledniiKapitalniiRemont, PosledniiKapitalniiRemontResponse,AktTehnObsledovaniya, AktTehnObsledovaniyaResponse,BalansPrinadlej, BalansPrinadlejResponse,InformacionnayaSystema, InformacionnayaSystemaResponse,InformacionnayaIPadress, InformacionnayaIPadressResponse,EnergoAudiot, EnergoAudiotResponse,KlassEnergo, KlassEnergoResponse,TehPasport, TehPasportResponse])
        stackViewObwieSvedeniya.axis = .vertical
        stackViewObwieSvedeniya.distribution = .fillEqually
        stackViewObwieSvedeniya.spacing = 7
        ScrollView.addSubview(stackViewObwieSvedeniya)
        stackViewObwieSvedeniya.anchor(top: ObwnieSvedeniyaMZHD.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 700)
//
//
        ScrollView.addSubview(Tehnharak)
        Tehnharak.anchor(top: stackViewObwieSvedeniya.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewTehHarakteristika = UIStackView(arrangedSubviews: [KolvoKvartir, KolvoKvartirResponse,GodPostroiki, GodPostroikiResponse,ObwayaPlowadDoma, ObwayaPlowadDomaResponse,ZhilayaPlowadDoma, ZhilayaPlowadDomaResponse,PlowadNejelih, PlowadNejelihResponse,PlowadPodvala, PlowadPodvalResponsea,PlowadCherdaka, PlowadCherdakaResponse,PlowadTehnEtaja, PlowadTehnEtajaResponse,KolvoEtajei, KolvoEtajeiResponse,KolvoPodezdov, KolvoPodezdovResponse,KolvoSekcii, KolvoSekciiResponse,PlowadMansardi, PlowadMansardiResponse,PlowadParkinga, PlowadParkingaResponse,SrokSlujbZdaniya, SrokSlujbZdaniyaResponse,IznosMZHD, IznosMZHDResponse])
        stackViewTehHarakteristika.axis = .vertical
        stackViewTehHarakteristika.distribution = .fillEqually
        stackViewTehHarakteristika.spacing = 7
        ScrollView.addSubview(stackViewTehHarakteristika)
         stackViewTehHarakteristika.anchor(top: Tehnharak.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 700)

        ScrollView.addSubview(NarujnieSteni)
        NarujnieSteni.anchor(top: stackViewTehHarakteristika.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil
            , right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewNarujnieSteni = UIStackView(arrangedSubviews: [Oblicovka, OblicovkaResponse,SostoyanieNaruknihsten, SostoyanieNaruknihstenResponse,FasadNarujnihSten, FasadNarujnihResponse,SostoyanieKato, SostoyanieKatoResponse])
        stackViewNarujnieSteni.axis = .vertical
        stackViewNarujnieSteni.distribution = .fillEqually
        stackViewNarujnieSteni.spacing = 7
        ScrollView.addSubview(stackViewNarujnieSteni)
        stackViewNarujnieSteni.anchor(top: NarujnieSteni.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 200)



        ScrollView.addSubview(FundamentLabel)
        FundamentLabel.anchor(top: stackViewNarujnieSteni.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor
            , paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewFUNDAMENT = UIStackView(arrangedSubviews: [MaterialFundament, MaterialFundamentResponse,SostoyanieFundament,SostoyanieFundamentResponse])
        stackViewFUNDAMENT.axis = .vertical
        stackViewFUNDAMENT.distribution = .fillEqually
        stackViewFUNDAMENT.spacing = 7
        ScrollView.addSubview(stackViewFUNDAMENT)
        stackViewFUNDAMENT.anchor(top: FundamentLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)


        ScrollView.addSubview(SteniPodvalaLabel)
        SteniPodvalaLabel.anchor(top: stackViewFUNDAMENT.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 00, paddingRight: 0, width: 0, height: 50)
        let stackViewSteniPodvala = UIStackView(arrangedSubviews: [MaterialStenPodvala, MaterialStenPodvalaResponse,SostoyanieStenPodvala,SostoyanieStenPodvalaResponse])
        stackViewSteniPodvala.axis = .vertical
        stackViewSteniPodvala.distribution = .fillEqually
        stackViewSteniPodvala.spacing = 7
        ScrollView.addSubview(stackViewSteniPodvala)
        stackViewSteniPodvala.anchor(top: SteniPodvalaLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)

        ScrollView.addSubview(PerekritiyaLabel)
        PerekritiyaLabel.anchor(top: stackViewSteniPodvala.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewPerekritiya = UIStackView(arrangedSubviews: [MaterialPerekritiya, MaterialPerekritiyaResponse,SostoyaniePerekritiya,SostoyaniePerekritiyaResponse])
        stackViewPerekritiya.axis = .vertical
        stackViewPerekritiya.distribution = .fillEqually
        stackViewPerekritiya.spacing = 7
        ScrollView.addSubview(stackViewPerekritiya)
        stackViewPerekritiya.anchor(top: PerekritiyaLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)

        ScrollView.addSubview(LestniciLabel)
        LestniciLabel.anchor(top: stackViewPerekritiya.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil
            , right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewPLestnici = UIStackView(arrangedSubviews: [SostoyanieLestnic, SostoyanieLestnicResponse,SostoyaniePeril,SostoyaniePerilResponse])
        stackViewPLestnici.axis = .vertical
        stackViewPLestnici.distribution = .fillEqually
        stackViewPLestnici.spacing = 7
        ScrollView.addSubview(stackViewPLestnici)
        stackViewPLestnici.anchor(top: LestniciLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)

        
        ScrollView.addSubview(BalkoniLabel)
        BalkoniLabel.anchor(top: stackViewPLestnici.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewBalkoni = UIStackView(arrangedSubviews: [SostoyanieBalkonov, SostoyanieBalkonovResponse])
        stackViewBalkoni.axis = .vertical
        stackViewBalkoni.distribution = .fillEqually
        stackViewBalkoni.spacing = 7
        ScrollView.addSubview(stackViewBalkoni)
        stackViewBalkoni.anchor(top: BalkoniLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)


        ScrollView.addSubview(KriwaLabel)
        KriwaLabel.anchor(top: stackViewBalkoni.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewKriwa = UIStackView(arrangedSubviews: [TipKriw, TipKriwResponse,MaterialKriw,MaterialKriwResponse,SostoyanieKRiwi,SostoyanieKRiwiResponse])
        stackViewKriwa.axis = .vertical
        stackViewKriwa.distribution = .fillEqually
        stackViewKriwa.spacing = 7
        ScrollView.addSubview(stackViewKriwa)
        stackViewKriwa.anchor(top: KriwaLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 150)


        ScrollView.addSubview(KrovlyaLabel)
        KrovlyaLabel.anchor(top: stackViewKriwa.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewKrovlya = UIStackView(arrangedSubviews: [MaterialKrovlya, MaterialKrovlyaResponse, SostoyanieKrovlya,SostoyanieKrovlyaResponse])
        stackViewKrovlya.axis = .vertical
        stackViewKrovlya.distribution = .fillEqually
        stackViewKrovlya.spacing = 7
        ScrollView.addSubview(stackViewKrovlya)
        stackViewKrovlya.anchor(top: KrovlyaLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)

        ScrollView.addSubview(MusoroprovodLabel)
        MusoroprovodLabel.anchor(top: stackViewKrovlya.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewMusoroprovod = UIStackView(arrangedSubviews: [NalichieMusoroprovoda, NalichieMusoroprovodaResponse, SostoyanieMusoroprovoda,SostoyanieMusoroprovodaResponse])
        stackViewMusoroprovod.axis = .vertical
        stackViewMusoroprovod.distribution = .fillEqually
        stackViewMusoroprovod.spacing = 7
        ScrollView.addSubview(stackViewMusoroprovod)
        stackViewMusoroprovod.anchor(top: MusoroprovodLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 80)

        
        ScrollView.addSubview(PodezdiLabel)
        PodezdiLabel.anchor(top: stackViewMusoroprovod.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewPodezdi = UIStackView(arrangedSubviews: [SostoyaniePodezd, SostoyaniePodezdResponse, ElektroLampi, ElektroLampiResponse, SostoyanieMusoroprovoda,SostoyanieMusoroprovodaResponse])
        stackViewPodezdi.axis = .vertical
        stackViewPodezdi.distribution = .fillEqually
        stackViewPodezdi.spacing = 7
        ScrollView.addSubview(stackViewPodezdi)
        stackViewPodezdi.anchor(top: PodezdiLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 120)

        
        ScrollView.addSubview(PodezdnieOknaLabel)
         PodezdnieOknaLabel.anchor(top: stackViewPodezdi.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewPodezdnieOkna = UIStackView(arrangedSubviews: [KolvoOkon, KolvoOkonResponse, MaterialOkon, MaterialOkonResponse, SostoyanieMusoroprovoda,SostoyanieMusoroprovodaResponse])
        stackViewPodezdnieOkna.axis = .vertical
        stackViewPodezdnieOkna.distribution = .fillEqually
        stackViewPodezdnieOkna.spacing = 7
        ScrollView.addSubview(stackViewPodezdnieOkna)
        stackViewPodezdnieOkna.anchor(top: PodezdnieOknaLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 150)

        
        ScrollView.addSubview(LiftiLabel)
        LiftiLabel.anchor(top: stackViewPodezdnieOkna.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewLifti = UIStackView(arrangedSubviews: [NalichieLifta,NalichieLiftaResponse,KolvoLiftov,KolvoLiftovResponse,StranaIzgLifta,StranaIzgLiftaResponse,SrokEkspluatLifta,SrokEkspluatLiftaResponse, DataUstanovkiLifta,DataUstanovkiLiftaResponse, DataPoslObslLifta, DataPoslObslLiftaResponse, ObslujOrganizaciya, ObslujOrganizaciyaResponse, TipOplatiLifta, TipOplatiLiftaResponse])
        stackViewLifti.axis = .vertical
        stackViewLifti.distribution = .fillEqually
        stackViewLifti.spacing = 7
        ScrollView.addSubview(stackViewLifti)
         stackViewLifti.anchor(top: LiftiLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 400)

        
        ScrollView.addSubview(TeplosabjenieLabel)
        TeplosabjenieLabel.anchor(top: stackViewLifti.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewTeplosnabjenie = UIStackView(arrangedSubviews: [TipTeplosnab,TipTeplosnabResponse, NomerCTP,NomerCTPResponse, KolvoTeplovihVvodov,KolvoTeplovihVvodovResponse, DiametrTruboProvoda,DiametrTruboProvodaResponse,NalichieATP,NalichieATPResponse, NalichieOPUTE, NalichieOPUTEResponse,KolvoOPUTE, KolvoOPUTEResponse, MarkaOPUTE,MarkaOPUTEResponse, DataPoslProverki,DataPoslProverkiResponse, PodderjkaDistancionnoi, PodderjkaDistancionnoiResponse, NalichieNasosa,NalichieNasosaResponse])
        stackViewTeplosnabjenie.axis = .vertical
        stackViewTeplosnabjenie.distribution = .fillEqually
        stackViewTeplosnabjenie.spacing = 7
        ScrollView.addSubview(stackViewTeplosnabjenie)
        stackViewTeplosnabjenie.anchor(top: TeplosabjenieLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 500)

        ScrollView.addSubview(VodoprovodLabel)
        VodoprovodLabel.anchor(top: stackViewTeplosnabjenie.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewVodoprovod = UIStackView(arrangedSubviews: [MaterialVopdoprovoda,MaterialVopdoprovodaResponse, DiametrVodoprovoda,DiametrVodoprovodaResponse, SostoyanieVodoprovoda,SostoyanieVodoprovodaResponse, NalichieOPU, NalichieOPUResponse,MarkaOPU,MarkaOPUTEResponse, PodderjkaDistancionnoiVodoprovoda,PodderjkaDistancionnoiVodoprovodaResponse, NalichieNasosaVodoprovoda, NalichieNasosaVodoprovodaResponse])
        stackViewVodoprovod.axis = .vertical
        stackViewVodoprovod.distribution = .fillEqually
        stackViewVodoprovod.spacing = 7
        ScrollView.addSubview(stackViewVodoprovod)
        stackViewVodoprovod.anchor(top: VodoprovodLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 300)

        
        ScrollView.addSubview(VodootvedenieLabel)
        VodootvedenieLabel.anchor(top: stackViewVodoprovod.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewVodootvedenie = UIStackView(arrangedSubviews: [MaterialVodootvedenie, MaterialVodootvedenieResponse, DiametrVodoprovoda, DiametrVodoprovodaResponse, SostoyanieVodoprovodaVodootvet, SostoyanieVodoprovodaVodootvetResponse])
        stackViewVodootvedenie.axis = .vertical
        stackViewVodootvedenie.distribution = .fillEqually
        stackViewVodootvedenie.spacing = 7
        ScrollView.addSubview(stackViewVodootvedenie)
        stackViewVodootvedenie.anchor(top: VodootvedenieLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 140)


        ScrollView.addSubview(GazosnobjenieLabel)
        GazosnobjenieLabel.anchor(top: stackViewVodootvedenie.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewGazosnabjenie = UIStackView(arrangedSubviews: [NalichieGazosnobjeniya,NalichieGazosnobjeniyaResponse])
        stackViewGazosnabjenie.axis = .vertical
        stackViewGazosnabjenie.distribution = .fillEqually
        stackViewGazosnabjenie.spacing = 7
        ScrollView.addSubview(stackViewGazosnabjenie)
        stackViewGazosnabjenie.anchor(top: GazosnobjenieLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)


        ScrollView.addSubview(ElektrosnobjenieLabel)
        ElektrosnobjenieLabel.anchor(top: stackViewGazosnabjenie.bottomAnchor, left: ScrollView.leftAnchor, bottom: nil, right: ScrollView.rightAnchor, paddingTop: 15, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        let stackViewElektrosnabjenie = UIStackView(arrangedSubviews: [NalichieOPUElektro,NalichieOPUElektroResponse, MarkaOPUElektro,MarkaOPUElektroResponse, PodderjkaDistancionnoiElektrosnobjenie,PodderjkaDistancionnoiElektrosnobjenieResponse,SostoyanieWitovih,SostoyanieWitovihResponse, TokoprovodyawieJili,TokoprovodyawieJiliResponse, WRC,WRCResponse])
        stackViewElektrosnabjenie.axis = .vertical
        stackViewElektrosnabjenie.distribution = .fillEqually
        stackViewElektrosnabjenie.spacing = 7
        ScrollView.addSubview(stackViewElektrosnabjenie)
        stackViewElektrosnabjenie.anchor(top: ElektrosnobjenieLabel.bottomAnchor, left: ScrollView.leftAnchor, bottom: ScrollView.bottomAnchor, right: ScrollView.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 300)
    }
}
