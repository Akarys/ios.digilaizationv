//
//  DisplayBuildingInfo.swift
//  ios.digitalization
//
//  Created by Akarys Turganbekuly on 05.09.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit
import JSONRPCKit
import ARKit

class DisplayBuildingInfo: UIViewController {
    
    var idwka: Int = 0
    
    lazy var contentScrollView: UIScrollView = {
        
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.contentSize.height = 2500
        return scrollView
    }()
    // All around photos
    
    let glavniIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let dvorovoiIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let praviiIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let leviiIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let glavniiLab: UILabel = {
        let label = UILabel()
        label.text = "Главный Фасад"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DvorovoiLab: UILabel = {
        let label = UILabel()
        label.text = "Дворовой Фасад"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let LeviiLab: UILabel = {
        let label = UILabel()
        label.text = "Левый торец фасада"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PraviiLab: UILabel = {
        let label = UILabel()
        label.text = "Правый торец фасада"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    
    
    // Adress information
    let Adress: UILabel = {
        let label = UILabel()
        label.text = "Адрес"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ulica: UILabel = {
        let label = UILabel()
        label.text = "Улица"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let Nomerdoma: UILabel = {
        let label = UILabel()
        label.text = "Номер дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let Index: UILabel = {
        let label = UILabel()
        label.text = "Индекс"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let oblast: UILabel = {
        let label = UILabel()
        label.text = "Область"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let gorod: UILabel = {
        let label = UILabel()
        label.text = "Город"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    //Common information about MZHD
    let ObwnieSvedeniyaMZHD: UILabel = {
        let label = UILabel()
        label.text = "Общие сведения"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let InvertarniiNomer: UILabel = {
        let label = UILabel()
        label.text = "Инвентарный номер дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KadastroviiNomer: UILabel = {
        let label = UILabel()
        label.text = "Кадастровый номер дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let RegestraciyaKondominimuma: UILabel = {
        let label = UILabel()
        label.text = "Регистрация Кондоминимума"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ObwayaPlowadUchastka: UILabel = {
        let label = UILabel()
        label.text = "Общая площадь земельного участка"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PosledniiKapitalniiRemont: UILabel = {
        let label = UILabel()
        label.text = "Последний капитальный ремонт"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let AktTehnObsledovaniya: UILabel = {
        let label = UILabel()
        label.text = "Акт технического обследования"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let BalansPrinadlej: UILabel = {
        let label = UILabel()
        label.text = "Балансовая принадлежность"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let InformacionnayaSystema: UILabel = {
        let label = UILabel()
        label.text = "Информационная Система"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let EnergoAudiot: UILabel = {
        let label = UILabel()
        label.text = "Энергоаудит"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KlassEnergo: UILabel = {
        let label = UILabel()
        label.text = "Класс Энергоэффективности"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let TehPasport: UILabel = {
        let label = UILabel()
        label.text = "Техпаспорт"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    //Tehnicheskaya harakteristika
    let Tehnharak: UILabel = {
        let label = UILabel()
        label.text = "Техническая Характеристика"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoKvartir: UILabel = {
        let label = UILabel()
        label.text = "Количество квартир"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let GodPostroiki: UILabel = {
        let label = UILabel()
        label.text = "Год постройки"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ObwayaPlowadDoma: UILabel = {
        let label = UILabel()
        label.text = "Общая площадь дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ZhilayaPlowadDoma: UILabel = {
        let label = UILabel()
        label.text = "Жилая площадь дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadNejelih: UILabel = {
        let label = UILabel()
        label.text = "Площадь нежелих помещений"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadPodvala: UILabel = {
        let label = UILabel()
        label.text = "Площадь подвала"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadCherdaka: UILabel = {
        let label = UILabel()
        label.text = "Площадь чердака"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadTehnEtaja: UILabel = {
        let label = UILabel()
        label.text = "Площадь технического этажа"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoEtajei: UILabel = {
        let label = UILabel()
        label.text = "Количество этажей"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoPodezdov: UILabel = {
        let label = UILabel()
        label.text = "Количество подъездов"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoSekcii: UILabel = {
        let label = UILabel()
        label.text = "Количество секций"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadMansardi: UILabel = {
        let label = UILabel()
        label.text = "Площадь мансарды"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PlowadParkinga: UILabel = {
        let label = UILabel()
        label.text = "Площадь паркинга"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SrokSlujbZdaniya: UILabel = {
        let label = UILabel()
        label.text = "Срок службы здания"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let IznosMZHD: UILabel = {
        let label = UILabel()
        label.text = "Износ МЖД"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    //Narujnie steni
    let NarujnieSteni: UILabel = {
        let label = UILabel()
        label.text = "Наружные стены"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NarujnieSteniIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let Oblicovka: UILabel = {
        let label = UILabel()
        label.text = "Облицовка"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieNaruknihsten: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let Fasad: UILabel = {
        let label = UILabel()
        label.text = "Фасад"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieKato: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    // Fundament Opisanie
    
    let Fundament: UILabel = {
        let label = UILabel()
        label.text = "Фундамент"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let FundamentIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialFundament: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieFundament: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Steni Podvala
    
    let SteniPodvala: UILabel = {
        let label = UILabel()
        label.text = "Стены Подвала"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SteniPodvalaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialStenPodvala: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieStenPodvala: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    // Perekritie
    let Perekritiya: UILabel = {
        let label = UILabel()
        label.text = "Перекрытия"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PerekritieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialPerekritiya: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyaniePerekritiya: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()

    //Lestnici
    let Lestnici: UILabel = {
        let label = UILabel()
        label.text = "Лестницы"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let LestniciIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let SostoyanieLestnic: UILabel = {
        let label = UILabel()
        label.text = "Состояние Лестниц"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyaniePeril: UILabel = {
        let label = UILabel()
        label.text = "Состояние Перил"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    // Балконныыыы
    
    let Balkoni: UILabel = {
        let label = UILabel()
        label.text = "Балконы"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let BalkoniIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let SostoyanieBalkonov: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Крыша
    let Kriwa: UILabel = {
        let label = UILabel()
        label.text = "Крыша"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KriwaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let TipKriw: UILabel = {
        let label = UILabel()
        label.text = "Тип"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MaterialKriw: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieKRiwi: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    // Кровля
    
    let Krovlya: UILabel = {
        let label = UILabel()
        label.text = "Кровля"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KrovlyaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialKrovlya: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieKrovlya: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Мусоропровод
    let Muskoroprovod: UILabel = {
        let label = UILabel()
        label.text = "Мусоропровод"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MusoroprovodIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let NalichieMusoroprovoda: UILabel = {
        let label = UILabel()
        label.text = "Наличие"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieMusoroprovoda: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    // Podezdi
    let Podezdi: UILabel = {
        let label = UILabel()
        label.text = "Подъезды"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodezdiIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let SostoyaniePodezd: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ElektroLampi: UILabel = {
        let label = UILabel()
        label.text = "Электросберигающие лампы"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //Podezdnie Okna
    let PodezdnieOkna: UILabel = {
        let label = UILabel()
        label.text = "Подъездные окна"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodezdnieOknaIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let KolvoOkon: UILabel = {
        let label = UILabel()
        label.text = "Количество"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MaterialOkon: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ///////////////////////Liftiiii
    let Lifti: UILabel = {
        let label = UILabel()
        label.text = "Лифты"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let LiftiIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let NalichieLifta: UILabel = {
        let label = UILabel()
        label.text = "Наличие"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoLiftov: UILabel = {
        let label = UILabel()
        label.text = "Количество"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let StranaIzgLifta: UILabel = {
        let label = UILabel()
        label.text = "Страна-изготовитель"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SrokEkspluatLifta: UILabel = {
        let label = UILabel()
        label.text = "Срок Эксплуатации"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataUstanovkiLifta: UILabel = {
        let label = UILabel()
        label.text = "Дата Установки"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataPoslObslLifta: UILabel = {
        let label = UILabel()
        label.text = "Дата последнего обследования"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ObslujOrganizaciya: UILabel = {
        let label = UILabel()
        label.text = "Обслуживащая Организация"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let TipOplatiLifta: UILabel = {
        let label = UILabel()
        label.text = "Тип оплаты"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    //////////////////////////// ТЕПЛОСНАБЖЕНИЕ
    
    let Teplosabjenie: UILabel = {
        let label = UILabel()
        label.text = "Теплоснабжение"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let TeplosnabjenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let TipTeplosnab: UILabel = {
        let label = UILabel()
        label.text = "Тип"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NomerCTP: UILabel = {
        let label = UILabel()
        label.text = "Номер ЦТП"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoTeplovihVvodov: UILabel = {
        let label = UILabel()
        label.text = "Количество тепловых вводов"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DiametrTruboProvoda: UILabel = {
        let label = UILabel()
        label.text = "Диаметр Трубопровода"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieATP: UILabel = {
        let label = UILabel()
        label.text = "Наличие АТП"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MarkaATP: UILabel = {
        let label = UILabel()
        label.text = "Марка АТП"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieOPUTE: UILabel = {
        let label = UILabel()
        label.text = "Наличие ОПУТЭ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let KolvoOPUTE: UILabel = {
        let label = UILabel()
        label.text = "Количество ОПУТЭ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MarkaOPUTE: UILabel = {
        let label = UILabel()
        label.text = "Марка ОПУТЭ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataPoslProverki: UILabel = {
        let label = UILabel()
        label.text = "Дата последней проверки"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodderjkaDistancionnoi: UILabel = {
        let label = UILabel()
        label.text = "Поддержка Дистанционной"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieNasosa: UILabel = {
        let label = UILabel()
        label.text = "Наличие Насоса"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ////////////////////// VODOPROVOD
    let Vodoprovod: UILabel = {
        let label = UILabel()
        label.text = "Водопровод"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let VodoprovodIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialVopdoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DiametrVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Диаметр Водопровода"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let MarkaOPU: UILabel = {
        let label = UILabel()
        label.text = "Марка ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieOPU: UILabel = {
        let label = UILabel()
        label.text = "Наличие ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodderjkaDistancionnoiVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Поддержка Дистанционной"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieNasosaVodoprovoda: UILabel = {
        let label = UILabel()
        label.text = "Наличие Насоса"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ///////////////////VODOOTVEDENIE
    let Vodootvedenie: UILabel = {
        let label = UILabel()
        label.text = "Водоотведение"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let VodootvedenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MaterialVodootvedenie: UILabel = {
        let label = UILabel()
        label.text = "Материал"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DiametrVodoprovodaVodootvet: UILabel = {
        let label = UILabel()
        label.text = "Диаметр Водопровода"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let SostoyanieVodoprovodaVodootvet: UILabel = {
        let label = UILabel()
        label.text = "Состояние"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    ///////////////////////////GAZOSNOBJENIE
    let Gazosnobjenie: UILabel = {
        let label = UILabel()
        label.text = "Газоснобжение"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let GazosnabjenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let NalichieGazosnobjeniya: UILabel = {
        let label = UILabel()
        label.text = "Наличие"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    /////////////////////////////ELEKTROSNABJENIE
    let Elektrosnobjenie: UILabel = {
        let label = UILabel()
        label.text = "Наличие Насоса"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let ElektrosnabjenieIV: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let MarkaOPUElektro: UILabel = {
        let label = UILabel()
        label.text = "Марка ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let NalichieOPUElektro: UILabel = {
        let label = UILabel()
        label.text = "Наличие ОПУ"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodderjkaDistancionnoiElektrosnobjenie: UILabel = {
        let label = UILabel()
        label.text = "Поддержка Дистанционной"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()

    let SostoyanieWitovih: UILabel = {
        let label = UILabel()
        label.text = "Состояние щитовых"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let TokoprovodyawieJili: UILabel = {
        let label = UILabel()
        label.text = "Токопроводящие жилы кабелей"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let WRC: UILabel = {
        let label = UILabel()
        label.text = "ШРС"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
//////////// POTREBLYAEMAYA MOWNOST DOMA
    let PotreblyaemayaMownost: UILabel = {
        let label = UILabel()
        label.text = "Потребляемая мощность дома"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
   ////////////////////////////////////////////////////////////// // Response that we will get to Label

 
    
    // Adress information
    let ulicaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NomerdomaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let IndexResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let oblastResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let gorodResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    //Common information about MZHD
    let InvertarniiNomerResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KadastroviiNomerResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let RegestraciyaKondominimumaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let ObwayaPlowadUchastkaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PosledniiKapitalniiRemontResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let AktTehnObsledovaniyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let BalansPrinadlejResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let InformacionnayaSystemaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let EnergoAudiotResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KlassEnergoResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let TehPasportResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    //Tehnicheskaya harakteristika
    let KolvoKvartirResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let GodPostroikiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let ObwayaPlowadDomaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let ZhilayaPlowadDomaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PlowadNejelihResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PlowadPodvalResponsea: UILabel = {
        let label = UILabel()
        return label
    }()
    let PlowadCherdakaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PlowadTehnEtajaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KolvoEtajeiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KolvoPodezdovResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KolvoSekciiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PlowadMansardiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PlowadParkingaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SrokSlujbZdaniyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let IznosMZHDResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    //Narujnie steni
    let OblicovkaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieNaruknihstenResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let FasadResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieKatoResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    // Fundament Opisanie
    let MaterialFundamentResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieFundamentResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    //Steni Podvala
    let MaterialStenPodvalaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieStenPodvalaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    // Perekritie

    let MaterialPerekritiyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyaniePerekritiyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    //Lestnici
    let SostoyanieLestnicResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyaniePerilResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    // Балконныыыы

    let SostoyanieBalkonovResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    //Крыша
    let TipKriwResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let MaterialKriwResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieKRiwiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    // Кровля
    
    let MaterialKrovlyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieKrovlyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    //Мусоропровод
    let NalichieMusoroprovodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieMusoroprovodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    // Podezdi
    let SostoyaniePodezdResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let ElektroLampiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    //Podezdnie Okna
    let KolvoOkonResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let MaterialOkonResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    ///////////////////////Liftiiii
    let NalichieLiftaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KolvoLiftovResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let StranaIzgLiftaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SrokEkspluatLiftaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let DataUstanovkiLiftaResponse: UILabel = {
        let label = UILabel()
        label.text = "Дата Установки"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let DataPoslObslLiftaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let ObslujOrganizaciyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let TipOplatiLiftaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    //////////////////////////// ТЕПЛОСНАБЖЕНИЕ

    let TipTeplosnabResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NomerCTPResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KolvoTeplovihVvodovResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let DiametrTruboProvodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NalichieATPResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let MarkaATPResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NalichieOPUTEResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let KolvoOPUTEResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let MarkaOPUTEResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let DataPoslProverkiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PodderjkaDistancionnoiResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NalichieNasosaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    ////////////////////// VODOPROVOD
    let MaterialVopdoprovodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let DiametrVodoprovodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieVodoprovodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let MarkaOPUResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NalichieOPUResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let PodderjkaDistancionnoiVodoprovodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NalichieNasosaVodoprovodaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    ///////////////////VODOOTVEDENIE
    let MaterialVodootvedenieResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let DiametrVodoprovodaVodootvetResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let SostoyanieVodoprovodaVodootvetResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    ///////////////////////////GAZOSNOBJENIE

    let NalichieGazosnobjeniyaResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    /////////////////////////////ELEKTROSNABJENIE
    let MarkaOPUElektroResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let NalichieOPUElektroResponse: UILabel = {
        let label = UILabel()
        
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    let PodderjkaDistancionnoiElektrosnobjenieResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let SostoyanieWitovihResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let TokoprovodyawieJiliResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    let WRCResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    //////////// POTREBLYAEMAYA MOWNOST DOMA
    let PotreblyaemayaMownostResponse: UILabel = {
        let label = UILabel()
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(contentScrollView)
        contentScrollView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        getBuildingInfo()
//        setupScrollView()
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
    func getBuildingInfo(){
        
        getResponse(o_method: "search_read", o_model: "property.building", o_domain: [["project_id", "=", idwka]], o_fields: [
            "street",
            "builded_at",
            "all_size",
            "land_size",
            "apartment_amount",
            "porch_amount",
            "residents_amoun",
            "level_amoun",
            "tech_passport",
            "is_tech_passport",
            "entrance_photo",
            "entrance_state",
            "entrance_energy_saving_lamps",
            "entrance_windows_count",
            "entrance_windows_material",
            "entrance_windows_photo",
            "lift_provided",
            "lift_amount",
            "lift_installed_at",
            "lift_lifetime",
            "lift_last_checked",
            "lift_company",
            "lift_payment_method",
            "lift_photo",
            "power_consumption",
            "last_meter_log_ids",
            "total_log_value",
            "manager_phone",
            "manager_email=",
            "ksk_notes",
            "sem_notes",
            "build_back_photo",
            "build_right_photo",
            "build_left_photo",
            "registration_of_condominium",
            "date_of_condominium",
            "land_act",
            "inventory_number",
            "cadastral_number",
            "date_of_the_last_overhaul",
            "date_of_the_technical_inspection_report",
            "conclusion_of_technical_inspection",
            "house_balance",
            "definition_isystem",
            "ip_address_isystem",
            "last_energy_audit_at",
            "putting_into_operation_at",
            "series_type_of_building_project",
            "number_of_sections",
            "has_attic",
            "number_of_parking_places",
            "service_life_of_the_building",
            "depreciation",
            "state",
            "automatic_lighting_control",
            "house_walls",
            "house_walls_state",
            "house_walls_photo",
            "house_walls_notes",
            "facade_type",
            "facade_type_state",
            "facade_type_notes",
            "facade_type_photo",
            "basement",
            "basement_state",
            "basement_notes",
            "basement_photo",
            "basement_wall",
            "basement_wall_state",
            "basement_wall_notes",
            "basement_wall_photo",
            "overlap",
            "overlap_state",
            "overlap_notes",
            "overlap_photo",
            "stairs_state",
            "stairs_notes",
            "stairs_photo",
            "railing_state",
            "railing_notes",
            "railing_photo",
            "balconies_state",
            "balconies_notes",
            "balconies_photo",
            "roof",
            "roof_material",
            "roof_state",
            "roof_notes",
            "roof_photo",
            "roofing",
            "roofing_state",
            "roofing_notes",
            "roofing_photo",
            "garbage_chute_state",
            "garbage_chute_notes",
            "garbage_chute_photo",
            "roof_type",
            "roofing_type",
            "intercom",
            "video_surveillance",
            "barrier",
            "telephony",
            "internet",
            "cable_tv",
            "hospital",
            "school",
            "kindergarten",
            "heat_supply_company",
            "water_supply_company",
            "power_supply_company",
            "gas_supply_company",
            "ventilation_company",
            "wastewater_company",
            "fire_safety_category",
            "fire_safety_evacuation",
            "fire_safety_system",
            "fire_safety_hydrant",
            "fire_safety_last_inspection",
            "chute_state",
            "loading_valve_covers",
            "chute_cleaning_devices",
            "chute_company",
            "chute_company_phone",
            "chute_price",
            "chute_additional_services",
            "sites_amount",
            "containers_amount",
            "waste_collection_schedule",
            "waste_company",
            "waste_company_phone",
            "waste_price",
            "waste_additional_services",
            "heat_type",
            "heat_type2",
            "ctp_tk",
            "thermal_inputs_count",
            "atp_brand",
            "atp",
            "atp_amount",
            "opute",
            "opute_amount",
            "opute_brand",
            "heat_remote_transmission",
            "heat_pipeline_diameter",
            "heat_last_check",
            "heat_pump",
            "water_pipeline_material",
            "water_pipeline_diameter",
            "water_pipeline_state",
            "water_opu_available",
            "water_opu_amount",
            "water_opu_brand",
            "remote_transmission",
            "water_pump",
            "wastewater_pipeline_diameter",
            "wastewater_pipeline_material",
            "wastewater_pipeline_state",
            "gas_supply",
            "power_supply_opu",
            "power_supply_opu_amount",
            "power_supply_opu_brand",
            "power_supply_remote_transmission",
            "power_supply_panel_state",
            "power_wires_type",
            "oput",
            "askuv",
            "askue",
            "date_ustanovka",
            "date_shanyraq",
            "date_ksk",
            "date_eiis_sem",
            "date_eiis_ksk",
            "transmission_technology",
            "employee_problem_questions",
            "parking_tech_passport",
            "parking_area",
            "parking_places",
            "security",
            "engineering_networks"
]) { (result) in
            
            print("MAIN RESULT", result)
    print(self.idwka)
            
            
            let qwe = result[0]["ip_address_isystem"].string
            print(qwe)
    
        }
    }
    
    
//    func setupScrollView(){
//
//        let stackViewModel = UIStackView(arrangedSubviews: [ModelSchetchika, ModelSchetchika])
//        stackViewModel.axis = .vertical
//        stackViewModel.distribution = .fillEqually
//        stackViewModel.spacing = 10
//        contentScrollView.addSubview(stackViewModel)
//        stackViewModel.anchor(top: contentScrollView.topAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)
//
//        let stackViewOblast = UIStackView(arrangedSubviews: [oblast, Oblast])
//        stackViewOblast.axis = .vertical
//        stackViewOblast.distribution = .fillEqually
//        stackViewOblast.spacing = 10
//        contentScrollView.addSubview(stackViewOblast)
//        stackViewOblast.anchor(top: stackViewModel.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//
//        let stackViewGorod = UIStackView(arrangedSubviews: [gorod, Gorod])
//        stackViewGorod.axis = .vertical
//        stackViewGorod.distribution = .fillEqually
//        stackViewGorod.spacing = 10
//        contentScrollView.addSubview(stackViewGorod)
//        stackViewGorod.anchor(top: stackViewOblast.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//
//
//        let stackViewKolvoIPU = UIStackView(arrangedSubviews: [ObweeKolvoipu, ObweeKolvoIPU])
//        stackViewKolvoIPU.axis = .vertical
//        stackViewKolvoIPU.distribution = .fillEqually
//        stackViewKolvoIPU.spacing = 10
//        contentScrollView.addSubview(stackViewKolvoIPU)
//        stackViewKolvoIPU.anchor(top: stackViewGorod.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewPodklKSEM = UIStackView(arrangedSubviews: [DataPodklksem, DataPodklKSEM])
//        stackViewPodklKSEM.axis = .vertical
//        stackViewPodklKSEM.distribution = .fillEqually
//        stackViewPodklKSEM.spacing = 10
//        contentScrollView.addSubview(stackViewPodklKSEM)
//        stackViewPodklKSEM.anchor(top: stackViewKolvoIPU.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewPodklKKSK = UIStackView(arrangedSubviews: [Datapodklkksk, DataPodklKKSK])
//        stackViewPodklKKSK.axis = .vertical
//        stackViewPodklKKSK.distribution = .fillEqually
//        stackViewPodklKKSK.spacing = 10
//        contentScrollView.addSubview(stackViewPodklKKSK)
//        stackViewPodklKKSK.anchor(top: stackViewPodklKSEM.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewMikroraion = UIStackView(arrangedSubviews: [mikroraion, Mikroraion])
//        stackViewMikroraion.axis = .vertical
//        stackViewMikroraion.distribution = .fillEqually
//        stackViewMikroraion.spacing = 10
//        contentScrollView.addSubview(stackViewMikroraion)
//        stackViewMikroraion.anchor(top: stackViewPodklKKSK.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewNomerDoma = UIStackView(arrangedSubviews: [Nomerdoma, NomerDoma])
//        stackViewNomerDoma.axis = .vertical
//        stackViewNomerDoma.distribution = .fillEqually
//        stackViewNomerDoma.spacing = 10
//        contentScrollView.addSubview(stackViewNomerDoma)
//        stackViewNomerDoma.anchor(top: stackViewMikroraion.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewKolvoKvartir = UIStackView(arrangedSubviews: [Kolvokvartir, KolvoKvartit])
//        stackViewKolvoKvartir.axis = .vertical
//        stackViewKolvoKvartir.distribution = .fillEqually
//        stackViewKolvoKvartir.spacing = 10
//        contentScrollView.addSubview(stackViewKolvoKvartir)
//        stackViewKolvoKvartir.anchor(top: stackViewNomerDoma.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewOPUT = UIStackView(arrangedSubviews: [oput, oPUT])
//        stackViewOPUT.axis = .vertical
//        stackViewOPUT.distribution = .fillEqually
//        stackViewOPUT.spacing = 10
//        contentScrollView.addSubview(stackViewOPUT)
//        stackViewOPUT.anchor(top: stackViewKolvoKvartir.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewATP = UIStackView(arrangedSubviews: [atp, aTP])
//        stackViewATP.axis = .vertical
//        stackViewATP.distribution = .fillEqually
//        stackViewATP.spacing = 10
//        contentScrollView.addSubview(stackViewATP)
//        stackViewATP.anchor(top: stackViewOPUT.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewDataUStan = UIStackView(arrangedSubviews: [Dataustanovki, DataUstanovki])
//        stackViewDataUStan.axis = .vertical
//        stackViewDataUStan.distribution = .fillEqually
//        stackViewDataUStan.spacing = 10
//        contentScrollView.addSubview(stackViewDataUStan)
//        stackViewDataUStan.anchor(top: stackViewATP.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewDataKShanyraq = UIStackView(arrangedSubviews: [DataPodklkshanyraq, DataPodklKShanyraq])
//        stackViewDataKShanyraq.axis = .vertical
//        stackViewDataKShanyraq.distribution = .fillEqually
//        stackViewDataKShanyraq.spacing = 10
//        contentScrollView.addSubview(stackViewDataKShanyraq)
//        stackViewDataKShanyraq.anchor(top: stackViewDataUStan.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//
//        let stackViewDataKeKSK = UIStackView(arrangedSubviews: [DataPodklKeksk, DataPodklKeKSK])
//        stackViewDataKeKSK.axis = .vertical
//        stackViewDataKeKSK.distribution = .fillEqually
//        stackViewDataKeKSK.spacing = 10
//        contentScrollView.addSubview(stackViewDataKeKSK)
//        stackViewDataKeKSK.anchor(top: stackViewDataKShanyraq.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewOtvetstvenniDolj = UIStackView(arrangedSubviews: [Otvetstvennidolj, OtvetstvenniDolj])
//        stackViewOtvetstvenniDolj.axis = .vertical
//        stackViewOtvetstvenniDolj.distribution = .fillEqually
//        stackViewOtvetstvenniDolj.spacing = 10
//        contentScrollView.addSubview(stackViewOtvetstvenniDolj)
//        stackViewOtvetstvenniDolj.anchor(top: stackViewDataKeKSK.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewOvetstvenniFIO = UIStackView(arrangedSubviews: [Ovetstvennifio, OvetstvenniFIO])
//        stackViewOvetstvenniFIO.axis = .vertical
//        stackViewOvetstvenniFIO.distribution = .fillEqually
//        stackViewOvetstvenniFIO.spacing = 10
//        contentScrollView.addSubview(stackViewOvetstvenniFIO)
//        stackViewOvetstvenniFIO.anchor(top: stackViewOtvetstvenniDolj.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewOtvetstvenniTEL = UIStackView(arrangedSubviews: [OtvetstvenniTELefon, OtvetstvenniTEL])
//        stackViewOtvetstvenniTEL.axis = .vertical
//        stackViewOtvetstvenniTEL.distribution = .fillEqually
//        stackViewOtvetstvenniTEL.spacing = 10
//        contentScrollView.addSubview(stackViewOtvetstvenniTEL)
//        stackViewOtvetstvenniTEL.anchor(top: stackViewOvetstvenniFIO.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewSemOrgan = UIStackView(arrangedSubviews: [Semorgan, SemOrgan])
//        stackViewSemOrgan.axis = .vertical
//        stackViewSemOrgan.distribution = .fillEqually
//        stackViewSemOrgan.spacing = 10
//        contentScrollView.addSubview(stackViewSemOrgan)
//        stackViewSemOrgan.anchor(top: stackViewOtvetstvenniTEL.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//        let stackViewSemFIO = UIStackView(arrangedSubviews: [Semfio, SemFIO])
//        stackViewSemFIO.axis = .vertical
//        stackViewSemFIO.distribution = .fillEqually
//        stackViewSemFIO.spacing = 10
//        contentScrollView.addSubview(stackViewSemFIO)
//        stackViewSemFIO.anchor(top: stackViewSemOrgan.bottomAnchor, left: contentScrollView.leftAnchor, bottom: nil, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//
//        let stackViewSemTel = UIStackView(arrangedSubviews: [SemTelefon, SemTel])
//        stackViewSemTel.axis = .vertical
//        stackViewSemTel.distribution = .fillEqually
//        stackViewSemTel.spacing = 10
//        contentScrollView.addSubview(stackViewSemTel)
//        stackViewSemTel.anchor(top: stackViewSemFIO.bottomAnchor, left: contentScrollView.leftAnchor, bottom: contentScrollView.bottomAnchor, right: contentScrollView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//
//    }
}
